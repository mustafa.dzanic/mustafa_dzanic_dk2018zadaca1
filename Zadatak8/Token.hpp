#pragma once

#include <iostream>
#include <string>

const int TAGBEGIN = 1;
const int TAGEND = 2;
const int TAGCLOSE = 3;
const int TAGENDANDCLOSE = 4;
const int ATTRIBUTENAME = 5;
const int EQUAL = 6;
const int ATTRIBUTEVALUE = 7;
const int CONTENT = 8;
const int WHITESPACE = 10;

class Token{
  public:
  int tag;
  std::string lexeme;

  Token(int i = 0, const std::string& s = "")
    : tag{i}, lexeme{s} {}

  Token& operator=(const Token& t)
  {
    tag = t.tag;
    lexeme = t.lexeme;
    return *this;
  }
  
  void print(size_t line, size_t column)
  {
    std::cout << "<" << getTagName(tag) << ", " << lexeme << ">" 
      << ": line " << line << ", column " << column << std::endl;
  }

  std::string getTagName(int tag)
  {
    switch (tag)
    {
      case 1:  return "TAGBEGIN";
      case 2:  return "TAGEND";
      case 3:  return "TAGCLOSE";
      case 4:  return "TAGENDCLOSE";
      case 5:  return "ATTRIBUTENAME";
      case 6:  return "EQUAL";
      case 7:  return "ATTRIBUTEVALUE";
      case 8:  return "CONTENT";
      case 10: return "WHITESPACE";
      default: return "unknown tag";
    }
  }
};
