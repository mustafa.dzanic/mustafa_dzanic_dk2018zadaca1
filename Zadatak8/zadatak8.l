
%{
#include <string>
#include <unordered_map>
#include "Token.hpp"
size_t column = 1;
size_t line = 1;
std::unordered_map<std::string, int> count;
%}

ws        [ \t\n]+
letter    [a-zA-Z]
digit     [0-9]
name      {letter}+
number    {digit}+
attribute {letter}({letter}|{digit})*

%%

{ws}          { column += yyleng; }
"<"{name}     { count["TAGBEGIN"]++;       return TAGBEGIN;       }
">"           { count["TAGEND"]++;         return TAGEND;         }
"</"{name}">" { count["TAGCLOSE"]++;       return TAGCLOSE;       }
"/>"          { count["TAGENDANDCLOSE"]++; return TAGENDANDCLOSE; }
{attribute}   { count["ATTRIBUTENAME"]++;  return ATTRIBUTENAME;  }
"="           { count["EQUAL"]++;          return EQUAL;          }
\".*\"        { count["ATTRIBUTEVALUE"]++; return ATTRIBUTEVALUE; }
.+            { count["CONTENT"]++;        return CONTENT;        }

%%

Token getToken() { return Token{yylex(), yytext}; }

int main()
{
  Token t;
  while ((t = getToken()).tag != 0)
  {
    if (line != yylineno)
    {
      column = 1;
      line = yylineno;
    }

    if (t.tag == ATTRIBUTEVALUE)
    {
      t.lexeme = std::string{t.lexeme, 1, t.lexeme.length() - 2};
      t.print(line, column + 1);
    } else
      t.print(line, column);
    
    column += yyleng;
  }
  
  std::cout << "TAGBEGIN: "       << count["TAGBEGIN"]       << std::endl;
  std::cout << "TAGEND: "         << count["TAGEND"]         << std::endl;
  std::cout << "TAGCLOSE: "       << count["TAGCLOSE"]       << std::endl;
  std::cout << "TAGENDANDCLOSE: " << count["TAGENDANDCLOSE"] << std::endl;
  std::cout << "ATTRIBUTENAME: "  << count["ATTRIBUTENAME"]  << std::endl;
  std::cout << "EQUAL: "          << count["EQUAL"]          << std::endl;
  std::cout << "ATTRIBUTEVALUE: " << count["ATTRIBUTEVALUE"] << std::endl;
  std::cout << "CONTENT: "        << count["CONTENT"]        << std::endl;

  return 0;
}

int yywrap() { return 1; }
