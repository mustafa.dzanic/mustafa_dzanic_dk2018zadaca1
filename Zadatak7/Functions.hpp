#pragma once

#include "Token.hpp"
#include "InputBuffer.hpp"

Token getTagBegin(InputBuffer&);
Token getTagEnd(InputBuffer&);
Token getTagClose(InputBuffer&);
Token getTagEndAndClose(InputBuffer&);
Token getAttributeName(InputBuffer&);
Token getEqual(InputBuffer&);
Token getAttributeValue(InputBuffer&);
Token getContent(InputBuffer&);
Token getWhitespace(InputBuffer&);
