#include "Functions.hpp"

bool insideTag = false;

Token getTagBegin(InputBuffer& buffer)
{
  size_t state = 0;
  buffer.startScan();
  char c;

  do {
    c = buffer.peekNext();
    switch (state)
    {
      case 0:
      if (c == '<')
        state = 1;
      else
        state = 99;
      break;
      
      case 1:
      if (isalpha(c))
        state = 2;
      else
        state = 99;
      break;
     
      case 2:
      if (!isalpha(c))
        return Token{TAGBEGIN, (insideTag = true, buffer.getLexeme())};
      else
        break;
      
      case 99:
      buffer.retreat();
      return Token{};
    }
  } while (buffer.getNext());

  return Token{};
}

Token getTagEnd(InputBuffer& buffer)
{
  size_t state = 0;
  buffer.startScan();
  char c;

  do {
    c = buffer.peekNext();
    switch (state)
    {
      case 0:
      if (c == '>')
        state = 1;
      else
        state = 99;
      break;
      
      case 1:
      return Token{TAGEND, (insideTag = false, buffer.getLexeme())};
      
      case 99:
      buffer.retreat();
      return Token{};
    }
  } while (buffer.getNext());

  return Token{};
}

Token getTagClose(InputBuffer& buffer)
{
  size_t state = 0;
  buffer.startScan();
  char c;

  do {
    c = buffer.peekNext();
    switch (state)
    {
      case 0:
      if (c == '<')
        state = 1;
      else
        state = 99;
      break;
      
      case 1:
      if (c == '/')
        state = 2;
      else
        state = 99;
      break;
      
      case 2:
      if (isalpha(c))
        state = 3;
      else
        state = 99;
      break;
     
      case 3:
      if (isalpha(c))
        break;
      else if (c == '>')
        state = 4;
      else
        state = 99;
      break;
      
      case 4:
      return Token{TAGCLOSE, buffer.getLexeme()};
      
      case 99:
      buffer.retreat();
      return Token{};
    }
  } while (buffer.getNext());

  return Token{};
}

Token getTagEndAndClose(InputBuffer& buffer)
{
  size_t state = 0;
  buffer.startScan();
  char c;

  do {
    c = buffer.peekNext();
    switch (state)
    {
      case 0:
      if (c == '/')
        state = 1;
      else
        state = 99;
      break;
      
      case 1:
      if (c == '>')
        state = 2;
      else
        state = 99;
      break;
      
      case 2:
      return Token{TAGENDANDCLOSE, buffer.getLexeme()};
      
      case 99:
      buffer.retreat();
      return Token{};
    }
  } while (buffer.getNext());

  return Token{};
}

Token getAttributeName(InputBuffer& buffer)
{
  if (!insideTag)
    return Token{};

  size_t state = 0;
  buffer.startScan();
  char c;

  do {
    c = buffer.peekNext();
    switch (state)
    {
      case 0:
      if (isalpha(c))
        state = 1;
      else
        state = 99;
      break;
     
      case 1:
      if (!isalpha(c) && !isdigit(c))
        return Token{ATTRIBUTENAME, buffer.getLexeme()};
      else
        break;
      
      case 99:
      buffer.retreat();
      return Token{};
    }
  } while (buffer.getNext());

  return Token{};
}

Token getEqual(InputBuffer& buffer)
{
  size_t state = 0;
  buffer.startScan();
  char c;

  do {
    c = buffer.peekNext();
    switch (state)
    {
      case 0:
      if (c == '=')
        state = 1;
      else
        state = 99;
      break;
      
      case 1:
      return Token{EQUAL, buffer.getLexeme()};
      
      case 99:
      buffer.retreat();
      return Token{};
    }
  } while (buffer.getNext());

  return Token{};
}

Token getAttributeValue(InputBuffer& buffer)
{
  size_t state = 0;
  buffer.startScan();
  char c;

  do {
    c = buffer.peekNext();
    switch (state)
    {
      case 0:
      if (c == '"')
        state = 1;
      else
        state = 99;
      break;
     
      case 1:
      buffer.startScan();
      state = 2;
      break;
      
      case 2:
      if (c == '"')
        state = 3;
      break;
      
      case 3:
      return Token{ATTRIBUTEVALUE, 
        std::string{buffer.getLexeme(), 0, buffer.getLexeme().length() - 1}}; 
      
      case 99:
      buffer.retreat();
      return Token{};
    }
  } while (buffer.getNext());

  return Token{};
}

Token getContent(InputBuffer& buffer)
{
  if (insideTag)
    return Token{};

  size_t state = 0;
  buffer.startScan();
  char c;

  do {
    c = buffer.peekNext();
    switch (state)
    {
      case 0:
      if (c == '<' || c == '>' || c == 0)
        return Token{CONTENT, buffer.getLexeme()};
      break;
      
      case 99:
      buffer.retreat();
      return Token{};
    }
  } while (buffer.getNext());

  return Token{}; 
}

Token getWhitespace(InputBuffer& buffer)
{
  buffer.startScan();
  char c = buffer.peekNext();

  if (c != ' ' && c != '\t' && c != '\n')
    return Token{};

  while ((c = buffer.getNext()) && (c == ' ' || c == '\t' || c == '\n'))
    ;

  if (c) buffer.backtrack();
  return Token{WHITESPACE, buffer.getLexeme()};
}
