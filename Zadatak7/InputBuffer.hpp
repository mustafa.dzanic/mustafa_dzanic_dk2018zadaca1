#pragma once

#include <string>

class InputBuffer
{
  public:
  InputBuffer(const std::string& text = "")
    : text_{text}, bufferLength_{text.length()} {}

  InputBuffer& operator=(const std::string& text)
  {
    text_ = text;
    lexemeBegin_ = currentIndex_ = 0;
    bufferLength_ = text.length();
    return *this;
  }

  bool empty() const { return currentIndex_ >= bufferLength_; }
  
  char peekNext() const
  { 
    if (!empty()) 
      return text_.at(currentIndex_); 
    else
      return 0;
  }
  char getNext()
  { 
    if (!empty()) 
      return text_.at(currentIndex_++); 
    else
      return 0;
  }
  
  std::string getLexeme() const { 
    return std::string{text_, lexemeBegin_, currentIndex_ - lexemeBegin_}; 
  }
  std::string getRest()
  {
    size_t index = currentIndex_;
    currentIndex_ = bufferLength_;
    return std::string{text_, index}; 
  }
  
  void startScan() { lexemeBegin_ = currentIndex_; }
  void retreat() { currentIndex_ = lexemeBegin_; }
  
  void retract(size_t n = 1) { if (currentIndex_ >= n) currentIndex_ -= n; }
  void backtrack(size_t n = 1) 
  { 
    if (!n) 
      currentIndex_ = lexemeBegin_; 
    else
      retract(n);
  }

  size_t getLexemeBegin() const { return lexemeBegin_; }

  private:
  std::string text_;
  size_t lexemeBegin_   = 0;
  size_t currentIndex_  = 0;
  size_t previousIndex_ = 0;
  size_t bufferLength_  = 0;
};
