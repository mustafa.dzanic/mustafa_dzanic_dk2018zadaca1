#include <iostream>
#include <unordered_map>
#include <string>

#include "main.hpp"

int main()
{
  std::unordered_map<std::string, size_t> count;

  size_t lineNumber = 0;
  std::string line;
  while (std::getline(std::cin, line))
  {
    InputBuffer buffer{line};
    while (!buffer.empty())
    {
      Token token;
      
      if ((token = getTagBegin(buffer)).tag)
        count["TAGBEGIN"]++;
      else if ((token = getTagEnd(buffer)).tag) 
        count["TAGEND"]++;
      else if ((token = getTagClose(buffer)).tag) 
        count["TAGCLOSE"]++;
      else if ((token = getTagEndAndClose(buffer)).tag)
        count["TAGENDANDCLOSE"]++;
      else if ((token = getAttributeName(buffer)).tag)
        count["ATTRIBUTENAME"]++;
      else if ((token = getEqual(buffer)).tag)
        count["EQUAL"]++;
      else if ((token = getAttributeValue(buffer)).tag)
        count["ATTRIBUTEVALUE"]++;
      else if ((token = getContent(buffer)).tag)
        count["CONTENT"]++;
      else if ((token = getWhitespace(buffer)).tag);
      else std::cout << "*** error at: " << buffer.getRest() << std::endl;
      
      if (token.tag && token.tag != WHITESPACE)
        token.print(lineNumber, buffer.getLexemeBegin());
      
    }
    ++lineNumber;
  }

  std::cout << "TAGBEGIN: "       << count["TAGBEGIN"]       << std::endl;
  std::cout << "TAGEND: "         << count["TAGEND"]         << std::endl;
  std::cout << "TAGCLOSE: "       << count["TAGCLOSE"]       << std::endl;
  std::cout << "TAGENDANDCLOSE: " << count["TAGENDANDCLOSE"] << std::endl;
  std::cout << "ATTRIBUTENAME: "  << count["ATTRIBUTENAME"]  << std::endl;
  std::cout << "EQUAL: "          << count["EQUAL"]          << std::endl;
  std::cout << "ATTRIBUTEVALUE: " << count["ATTRIBUTEVALUE"] << std::endl;
  std::cout << "CONTENT: "        << count["CONTENT"]        << std::endl;

  return 0;
}
